import os.path
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
import csv
from unidecode import unidecode  

SCOPES = ["https://www.googleapis.com/auth/spreadsheets.readonly"]

SPREADSHEET_ID = '13JC_KQ_aZVf9YfoKDuxxoRFjn0HyxvoegTkHAbvIMUI'
RANGE_NAME = 'Réponses au formulaire 1!B1:G'

def main():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet and writes them to a CSV file, removing accents.
    """
    creds = None

    if os.path.exists("token.json"):
        creds = Credentials.from_authorized_user_file("token.json", SCOPES)

    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                "credentials.json", SCOPES
            )
            creds = flow.run_local_server(port=0)

        with open("token.json", "w") as token:
            token.write(creds.to_json())

    try:
        service = build("sheets", "v4", credentials=creds)

        sheet = service.spreadsheets()
        result = (
            sheet.values()
            .get(spreadsheetId=SPREADSHEET_ID, range=RANGE_NAME)
            .execute()
        )
        values = result.get("values", [])

        if not values:
            print("No data found.")
            return

        print("Voici les données (avec accents supprimés):")
        for row in values:
            row = [unidecode(str(cell)) for cell in row]
            print(f"{row}")

        with open("donnees_sheet.csv", "w", newline='', encoding='utf-8') as csvfile:
            csvwriter = csv.writer(csvfile)
            for row in values:
                row = [unidecode(str(cell)) for cell in row]
                csvwriter.writerow(row)

        print("Les données ont été écrites dans 'donnees_sheet.csv'.")

    except HttpError as err:
        print(err)


if __name__ == "__main__":
    main()