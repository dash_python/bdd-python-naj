DROP DATABASE IF EXISTS sondage;
CREATE DATABASE sondage;
USE sondage;


CREATE TABLE pays (
  id_pays INT AUTO_INCREMENT PRIMARY KEY,
  pays VARCHAR(50) NOT NULL
);

CREATE TABLE personnes (
  id_personne INT AUTO_INCREMENT PRIMARY KEY,
  nom VARCHAR(50) NOT NULL,
  prenom VARCHAR(50) NOT NULL,
  email VARCHAR(100) NOT NULL,
  pays_residence INT,
  FOREIGN KEY (pays_residence) REFERENCES pays(id_pays)
);

CREATE TABLE voyage (
  id_personne INT NOT NULL,
  id_pays INT NOT NULL,
  duree INT NOT NULL,
  FOREIGN KEY (id_personne) REFERENCES personnes(id_personne),
  FOREIGN KEY (id_pays) REFERENCES pays(id_pays)
);