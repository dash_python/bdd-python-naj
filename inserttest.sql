USE sondagetest;

INSERT INTO pays (id_pays, pays) VALUES
(1, 'France'),
(2, 'Allemagne'),
(3, 'Italie'),
(4, 'Espagne'),
(5, 'Portugal'),
(6, 'Belgique'),
(7, 'Suisse'),
(8, 'Autriche'),
(9, 'Pays-Bas'),
(10, 'Suède');

INSERT INTO personnes (id_personne, nom, prenom, email, pays_residence) VALUES
(1, 'Martin', 'Lucas', 'lucas.martin@example.com', 1),
(2, 'Bernard', 'Émilie', 'emilie.bernard@example.com', 2),
(3, 'Dubois', 'Julien', 'julien.dubois@example.com', 3),
(4, 'Thomas', 'Sophie', 'sophie.thomas@example.com', 4),
(5, 'Robert', 'Clara', 'clara.robert@example.com', 5),
(6, 'Richard', 'Nicolas', 'nicolas.richard@example.com', 6),
(7, 'Petit', 'Julie', 'julie.petit@example.com', 7),
(8, 'Durand', 'Alexandre', 'alexandre.durand@example.com', 8),
(9, 'Leroy', 'Marine', 'marine.leroy@example.com', 9),
(10, 'Moreau', 'Christophe', 'christophe.moreau@example.com', 10);

INSERT INTO voyage (id_personne, id_pays, duree) VALUES
(1, 5, 10),
(2, 4, 20),
(3, 3, 30),
(4, 2, 40),
(5, 1, 50),
(6, 10, 60),
(7, 9, 70),
(8, 8, 80),
(9, 7, 90),
(10, 6, 100),
(1, 2, 15),
(2, 3, 25),
(3, 4, 35),
(4, 5, 45),
(5, 6, 55),
(6, 7, 65),
(7, 8, 75),
(8, 9, 85),
(9, 10, 95),
(10, 1, 5);
