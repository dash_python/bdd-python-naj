import mysql.connector
from mysql.connector import Error
import pandas as pd

def inserer_donnees(donnees_path):
    try:
        connection = mysql.connector.connect(
            host='localhost',
            database='sondage',
            user='alucard',
            password='coucou123')
        if connection.is_connected():
            
            donnees = pd.read_csv(donnees_path)
            
            pays_uniques = pd.concat([donnees['pays_residence'], donnees['pays_visite']]).unique()
            for pays in pays_uniques:
                cursor = connection.cursor(buffered=True)
                cursor.execute("INSERT INTO pays (pays) VALUES (%s) ON DUPLICATE KEY UPDATE pays=pays;", (pays,))
                cursor.close()
            connection.commit()
            
            for _, row in donnees.iterrows():
                cursor = connection.cursor(buffered=True)
                cursor.execute("SELECT id_pays FROM pays WHERE pays = %s;", (row['pays_residence'],))
                id_pays_residence = cursor.fetchone()[0]
                
                cursor.execute("INSERT INTO personnes (nom, prenom, email, pays_residence) VALUES (%s, %s, %s, %s);",
                               (row['Nom'], row['Prenom'], row['Email'], id_pays_residence))
                cursor.close()
                connection.commit()
            
            for _, row in donnees.iterrows():
                cursor = connection.cursor(buffered=True)
                cursor.execute("SELECT id_personne FROM personnes WHERE email = %s;", (row['Email'],))
                id_personne = cursor.fetchone()[0]
                cursor.execute("SELECT id_pays FROM pays WHERE pays = %s;", (row['pays_visite'],))
                id_pays_visite = cursor.fetchone()[0]
                
                cursor.execute("INSERT INTO voyage (id_personne, id_pays, duree) VALUES (%s, %s, %s);",
                               (id_personne, id_pays_visite, row['Duree']))
                cursor.close()
                connection.commit()
            
    except Error as e:
        print(f"Erreur lors de la connexion à MySQL ou pendant l'exécution : {e}")
    finally:
        if connection and connection.is_connected():
            connection.close()

inserer_donnees('donnees_sheet.csv')