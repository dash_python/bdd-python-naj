# Exercice BDD - PYTHON

## création du projet pour récuprer les données du spreadsheets

allez sur ce lien et suivez les instructions : https://developers.google.com/sheets/api/quickstart/python?hl=fr

## récupération des données du spreadsheets transformation en csv

lancez le script.py, ce script récupére les données du spreadsheets, les normalise et crée un fichier csv : donnees_sheet.csv
Pensez à remplacer dans le fichier script.py les lignes : 

```py
SPREADSHEET_ID = '13JC_KQ_aZVf9YfoKDuxxoRFjn0HyxvoegTkHAbvIMUI'
RANGE_NAME = 'Réponses au formulaire 1!B1:G'
```
par les lignes correspondates à votre spreadsheets et à la feuille de votre spreadsheets.

## lancer la BDD

lancer le fichier bdd.sql

```sh
sudo mysql < bdd.sql
```

lancer le fichier qui donne les accées (penser à bien lire le fichier avant de le lancer)

```sh
sudo mysql < acces.sql
```

## BDD de test 

lancer le fichier bddtest.sql

```sh
sudo mysql < bddtest.sql
```

lancer le fichier qui donne les accées à la bdd de test (penser à bien lire le fichier avant de le lancer)

```sh
sudo mysql < accestest.sql
```

lancer le fichier d'insertion de données

```sh
sudo mysql < inserttest.sql
```

## insertion des données dans la bdd

lancez le fichier insertintobdd.py, cela récupérera les données du fichier donneesès_sheet.csv et les ajoutera dans la bdd nouvellement crée.
Vérifiez bien que le chemin du fichier est le bon tout en bas dans la ligne :

```py
inserer_donnees('donnees_sheet.csv')
```
Pensez aussi à changer les informations dans la partie mysql.connector avec vos propres informations.